# Removing/deleting messages

## via average IRC client

1. `/cap req message-tags` or set it to be automatically requested
   (WeeChat: `irc.server_default.capabilities`)
1. Open raw log (WeeChat: `/server raw`)
1. `/quote history #channel` and look for the spam message alongside it
   `@msgid=qwerty123456789`
1. `/msg histserv delete qwerty123456789` (using the msgid here`)

## via KiwiIRC

* https://irc2.piraattipuolue.fi/

Method 1: Right-click the message and select "Inspect Element" to find
the `data-message-id`. Paste it into `/msg histserv delete`.

Method 2:

1. Open Settings from the top left corner (where there is the first
   character of your display name).
1. Scroll to bottom and enable advanced settings.
1. Enable `showRaw` (feel free to filter for it).
1. Follow the average IRC client instructions starting from step 3.
