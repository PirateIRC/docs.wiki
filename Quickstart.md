# Quickstart

The following is hints on how to get started with this network.

## Registration

Please consider registering your nickname in order to benefit of the
integrated bouncer and other modern features: `/MSG NICKSERV REGISTER PASSWORDHERE EMAIL@EXAMPLE.NET`

## Login

Login is best done by using SASL to ensure you get access to your registered
NickName. Libera Chat has the most comprehensive documentation on how to enable
it at https://libera.chat/guides/sasl

TIP: add `@identifier` e.g. `@PhoneClient` after your (SASL) username to separate the
     different locations/clients and utilise client-specific playback buffers.
     This will also stop repeating messages (as long as you won't change the
     `@identifier`)!

If SASL isn't supported, set server password (PASS command) as `username@identifier:password`,
it should be supported by all clients.

## Integrated bouncer

As a registered user, should you wish, you can configure the network to
allow you to receive channel/private messages even when you have no clients
connected. This is known as a bouncer.

Keep your nickname connected and receiving messages: `/MSG NICKSERV SET ALWAYS-ON ON`

TIP: If your client supports ZNC's Playback module, it will automatically
     handle receiving only missed lines. See also https://wiki.znc.in/Playback#Supported_clients
     for list of them and scripts.
TIP: If you are running a relaybot, you WANT TO USE `@CLIENTID` IN SASL USERNAME
     to avoid repeating messages and ignore `HistServ` which only includes automated
     state change messages.

For more informations on retrieving messages,
[please refer to history section of the upstream manual](https://github.com/ergochat/ergo/blob/master/docs/MANUAL.md#history).

## Tor access

Refer to https://pirateirc.net/ or MOTD. MapAddress and SASL recommended.

## Further reading

* Upstream user guide: https://github.com/ergochat/ergo/blob/master/docs/USERGUIDE.md
  * Upstream manual: https://github.com/ergochat/ergo/blob/master/docs/MANUAL.md (mostly aimed at operators)
* The Ergo IRCd integrated documentation: `/HELPOP INDEX`
  * On some clients the command may be: `/QUOTE HELPOP INDEX`
    or `/RAW HELPOP INDEX`
  in order to send the command directly to server and not process it locally.
* Nickname management: `/MSG NICKSERV HELP`
* Channel management: `/MSG CHANSERV HELP`
* Vhost management: `/MSG HOSTERV HELP`
* History management: `/MSG HISTSERV HELP`
